/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023-2024  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package user

import (
  e "vidhukant.com/openbills/errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

func handleGetUser (ctx *gin.Context) {
	var user User

  uId, ok := ctx.Get("UserID")
  if !ok {
    ctx.Error(e.ErrUnauthorized)
    ctx.Abort()
    return
  }

  userId := uId.(uint)

  err := GetUser(&user, userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

  // remove password hash from response
  user.Password = ""

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": user,
	})
}

func handleDelUser (ctx *gin.Context) {
	id := uint(1) // get from JWT

	var user User
	user.ID = id

	// TODO: add a verification mechanism
	err := user.del()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}
