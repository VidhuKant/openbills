/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023-2024  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package user

import (
	d "vidhukant.com/openbills/db"
	e "vidhukant.com/openbills/errors"
  u "vidhukant.com/openbills/util"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"github.com/spf13/viper"
	"errors"
)

var COST int
var db *gorm.DB
func init() {
	db = d.DB

	db.AutoMigrate(&User{})

	COST = viper.GetInt("cryptography.password_hashing_cost")
}

type User struct {
	gorm.Model
  u.Address
  FullName   string
  FirmName   string
  Gstin      string
  Phone      string
  Email      string
  Website    string
	Username   string
	Password   string
	IsVerified bool
}

func CheckPassword(user *User, accountName, method, pass string) error {
	err := GetUserWithAccountName(user, accountName, method)
	if err != nil {
		return err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(pass))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return e.ErrWrongPassword
		}

		return err
	}

	return nil
}
