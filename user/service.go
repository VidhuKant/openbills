/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package user

import (
	e "vidhukant.com/openbills/errors"
)

func (u *User) Create() error {
	res := db.Create(u)
	// TODO: handle potential errors
	return res.Error
}

func GetUserWithAccountName(user *User, accountName, method string) error {
	if method != "username" && method != "email" {
		return e.ErrInvalidLoginMethod
	}

	res := db.Where(method + " = ?", accountName).Find(&user)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}

func GetUser(user *User, id uint) error {
	res := db.Find(&user, id)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}

func (u *User) del() error {
	res := db.Delete(u)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}
