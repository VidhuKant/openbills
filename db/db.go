/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package db

import (
	"gorm.io/gorm"
	"gorm.io/driver/mysql"
	"github.com/spf13/viper"
	"log"
	"fmt"
)

var DB *gorm.DB

func init() {
	conf := viper.Get("database").(map[string]interface{})

	x := fmt.Sprintf(
		"%v:%v@%v/%v?%v",
		conf["username"],
		conf["password"],
		conf["location"],
		conf["db_name"],
		conf["params"],
	)

	var err error
	DB, err = gorm.Open(mysql.Open(x), &gorm.Config{})

	if err != nil {
		log.Fatalf("\x1b[41m\x1b[30m[err]\x1b[0m cannot to connect to database, exiting.\n%v\n", err)
	}

	log.Printf("\x1b[46m\x1b[30m[info]\x1b[0m Successfully Connected To Database \"%v\"\n", conf["db_name"])
}
