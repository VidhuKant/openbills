/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"net/http"
)

type Info struct {
	Version     string
	Title       string
	Description string
	URL         string
}

func serverInfo(ctx *gin.Context) {
	info := Info {
	  Version:     OPENBILLS_VERSION,
		Title:       viper.GetString("instance.title"),
		Description: viper.GetString("instance.description"),
		URL:         viper.GetString("instance.url"),
	}

	ctx.JSON(http.StatusOK, info)
}
