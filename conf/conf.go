/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package conf

import (
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
	"log"
)

func validateConf() {
	ok := true
	log.Println("\x1b[46m\x1b[30m[info]\x1b[0m Checking errors in config file...")

	port := viper.GetInt("port")
	if port < 1024 || port > 65535 {
		log.Println("\x1b[41m\x1b[30m[err]\x1b[0m Invalid port number.")
		ok = false
	}

	if viper.GetInt("username.min_username_length") < 1 {
		log.Println("\x1b[41m\x1b[30m[err]\x1b[0m Minimum username length must be greater than 0.")
		ok = false
	}

	minPassLen := viper.GetInt("security.min_password_length")
	maxPassLen := viper.GetInt("security.max_password_length")

	if minPassLen < 1 {
		log.Println("\x1b[41m\x1b[30m[err]\x1b[0m Minimum password length must be greater than 0.")
		ok = false
	}

	if maxPassLen > 72 {
		log.Println("\x1b[41m\x1b[30m[err]\x1b[0m Maximum password length can't exceed 72 characters.")
		ok = false
	}

	if minPassLen > maxPassLen {
		log.Println("\x1b[41m\x1b[30m[err]\x1b[0m Minimum password length can't be greater than maximum password length.")
		ok = false
	}

	hashingCost := viper.GetInt("cryptography.password_hashing_cost")

	if hashingCost > bcrypt.MaxCost {
		log.Printf("\x1b[41m\x1b[30m[err]\x1b[0m Password hashing cost can't be greater than %d.\n", bcrypt.MaxCost)
		ok = false
	}

	if hashingCost < bcrypt.MinCost {
		log.Printf("\x1b[41m\x1b[30m[err]\x1b[0m Password hashing cost can't be less than %d.\n", bcrypt.MinCost)
		ok = false
	}

	if !ok {
		log.Fatalln("\x1b[41m\x1b[30m[err]\x1b[0m Config file has errors.")
	}
}

func init() {
	viper.SetConfigName("openbills")
	viper.AddConfigPath("/etc/openbills")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		log.Printf("\x1b[41m\x1b[30m[err]\x1b[0m Failed to read config file:\n")
		log.Fatalf("%v\n", err.Error())
	}

	viper.SetDefault("port", "8765")
	viper.SetDefault("debug_mode", false)

	viper.SetDefault("security.min_password_length", 12)
	viper.SetDefault("security.max_password_length", 72)

	viper.SetDefault("instance.title", "OpenBills")
	viper.SetDefault("instance.description", "Libre Billing Software")
	viper.SetDefault("instance.url", "https://openbills.vidhukant.com")

	viper.SetDefault("username.allowed_characters", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_")
	viper.SetDefault("username.min_username_length", 2)
	viper.SetDefault("username.max_username_length", 20)

	viper.SetDefault("cryptography.password_hashing_cost", bcrypt.DefaultCost)

	validateConf()
	log.Printf("\x1b[46m\x1b[30m[info]\x1b[0m Loaded Config \"%s\"\n", viper.ConfigFileUsed())
}
