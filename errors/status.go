/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package errors

import (
	"errors"
	"net/http"
)

func StatusCodeFromErr(err error) int {
	// 204
	if errors.Is(err, ErrEmptyResponse) {
		return http.StatusNoContent
	}

	// 400
	if errors.Is(err, ErrNoWhereCondition) ||
		errors.Is (err, ErrInvalidID) ||
		errors.Is (err, ErrEmptyContactName) ||
		errors.Is(err, ErrInvalidGSTIN) ||
		errors.Is(err, ErrInvalidEmail) ||
		errors.Is(err, ErrInvalidPhone) ||
		errors.Is(err, ErrInvalidWebsite) ||
		errors.Is(err, ErrEmptyBrandName) ||
		errors.Is(err, ErrInvalidUnitPrice) ||
		errors.Is(err, ErrPasswordTooShort) ||
		errors.Is(err, ErrPasswordTooLong) ||
		errors.Is(err, ErrInvalidLoginMethod) ||
		errors.Is(err, ErrInvalidGSTPercentage) {
		return http.StatusBadRequest
	}

	// 401
	if errors.Is(err, ErrWrongPassword) ||
		errors.Is(err, ErrUnauthorized) ||
		errors.Is(err, ErrInvalidAuthHeader) ||
		errors.Is(err, ErrSessionExpired) {
		return http.StatusUnauthorized
	}

	// 403
	if errors.Is(err, ErrForbidden) {
		return http.StatusForbidden
	}

	// 404
	if errors.Is(err, ErrNotFound) ||
		errors.Is(err, ErrBrandNotFound) {
		return http.StatusNotFound
	}

	// 405
	if errors.Is(err, ErrCannotEditInvoice) {
		return http.StatusMethodNotAllowed
	}

	// 409
	if errors.Is(err, ErrNonUniqueGSTIN) ||
		errors.Is(err, ErrNonUniquePhone) ||
		errors.Is(err, ErrNonUniqueUsername) ||
		errors.Is(err, ErrNonUniqueEmail) ||
		errors.Is(err, ErrNonUniqueWebsite) ||
		errors.Is(err, ErrNonUniqueBrandName) ||
		errors.Is(err, ErrNonUniqueInvoiceNumber) ||
		errors.Is(err, ErrNonUniqueBrandItem) {
		return http.StatusConflict
	}

	// 500
	if errors.Is(err, ErrInternalServerError) {
		return http.StatusInternalServerError
	}

	return http.StatusInternalServerError
}
