/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package errors

import (
	"github.com/gin-gonic/gin"
	"log"
)

func writeLog(method, path string, statusCode int, err error) {
	var prefix string
	if (statusCode >= 400 && statusCode < 500) || (statusCode >= 200 && statusCode < 300) {
		prefix = "\x1b[43m\x1b[30m[warn]\x1b[0m"
	} else {
		prefix = "\x1b[41m\x1b[30m[err]\x1b[0m"
	}

	log.Printf("%s \x1b[47m\x1b[30m%s %s\x1b[0m %d %s", prefix, method, path, statusCode, err.Error())
}

func ErrResponse() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()

		if len(ctx.Errors) > 0 {
			err := ctx.Errors[0].Err

			status := ctx.Writer.Status()
			if status == 200 {
				status = StatusCodeFromErr(err)
			}

			ctx.JSON(status, gin.H{
				"error": err.Error(),
			})

			writeLog(
				ctx.Request.Method,
				ctx.FullPath(),
				status,
				err,
			)
		}
	}
}
