/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023-2024  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package errors

import (
	"errors"
)

var (
	// 204
	ErrEmptyResponse          = errors.New("No Records Found")

	// 400
	ErrNoWhereCondition       = errors.New("No Where Condition")
	ErrInvalidID              = errors.New("Invalid ID")
	ErrEmptyContactName       = errors.New("Contact Name Cannot Be Empty")
	ErrInvalidGSTIN           = errors.New("Invalid GSTIN")
	ErrInvalidEmail           = errors.New("Invalid E-Mail Address")
  ErrEmptyEmail             = errors.New("E-Mail Address Cannot Be Empty")
  ErrInvalidUsername        = errors.New("Invalid Username")
  ErrEmptyUsername          = errors.New("Username Cannot Be Empty")
	ErrInvalidPhone           = errors.New("Invalid Phone Number")
	ErrInvalidWebsite         = errors.New("Invalid Website URL")
	ErrEmptyBrandName         = errors.New("Brand Name Cannot Be Empty")
	ErrInvalidUnitPrice       = errors.New("Invalid Unit Price")
	ErrInvalidGSTPercentage   = errors.New("Invalid GST Percentage")
	ErrPasswordTooShort       = errors.New("Password Is Too Short")
	ErrPasswordTooLong        = errors.New("Password Is Too Long")
	ErrUsernameTooShort       = errors.New("Username Is Too Short")
	ErrUsernameTooLong        = errors.New("Username Is Too Long")
	ErrInvalidLoginMethod     = errors.New("Login Method Can Only Be 'email' Or 'username'")

	// 401
	ErrWrongPassword          = errors.New("Wrong Password")
	ErrInvalidAuthHeader      = errors.New("Invalid Authorization Header")
	ErrUnauthorized           = errors.New("Unauthorized")
	ErrSessionExpired         = errors.New("Session Expired")

	// 403
	ErrForbidden              = errors.New("You Are Not Authorized To Access This Resource")

	// 404
	ErrNotFound               = errors.New("Not Found")
	ErrBrandNotFound          = errors.New("This Brand Does Not Exist")

	// 405
	ErrCannotEditInvoice      = errors.New("This Invoice Cannot Be Edited Now")

	// 409
	ErrNonUniqueGSTIN         = errors.New("GSTIN Must Be Unique")
	ErrNonUniquePhone         = errors.New("Phone Number Is Already In Use")
	ErrNonUniqueEmail         = errors.New("Email Address Is Already In Use")
	ErrNonUniqueUsername      = errors.New("Username Is Already In Use")
	ErrNonUniqueWebsite       = errors.New("Website Is Already In Use")
	ErrNonUniqueBrandName     = errors.New("Brand Name Is Already In Use")
	ErrNonUniqueBrandItem     = errors.New("Item With Same Name And Brand Already Exists")
	ErrNonUniqueInvoiceNumber = errors.New("Invoice Number Must Be Unique")

	// 500
	ErrInternalServerError    = errors.New("Internal Server Error")
)
