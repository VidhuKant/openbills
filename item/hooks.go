/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package item

import (
	"gorm.io/gorm"
	"vidhukant.com/openbills/errors"
	e "errors"
)

func (i *SavedItem) BeforeSave(tx *gorm.DB) error {
	var err error

	// also checks if brand actually exists
	err = checkBrandOwnership(i.BrandID, i.UserID)
	if err != nil {
		if e.Is(err, errors.ErrBrandNotFound) {
			// this error has a better error message for this case
			return errors.ErrBrandNotFound
		}
		return err
	}

	err = i.validate()
	if err != nil {
		return err
	}

	return nil
}

func (b *Brand) BeforeSave(tx *gorm.DB) error {
	err := b.validate()
	if err != nil {
		return err
	}

	return nil
}

func (b *Brand) BeforeDelete(tx *gorm.DB) error {
  // if ID is 0, brand won't be deleted
	if b.ID == 0 {
		return errors.ErrNoWhereCondition
	}

	// delete all items
	err := db.Where("brand_id = ? and user_id = ?", b.ID, b.UserID).Delete(&SavedItem{}).Error
	if err != nil {
		return err
	}

	return nil
}
