/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package item

import (
	"vidhukant.com/openbills/errors"
)

func getBrandItems(items *[]SavedItem, id, userId uint) error {
	err := checkBrandOwnership(id, userId)
	if err != nil {
		return err
	}

	// get items
	res := db.Model(&SavedItem{}).Where("brand_id = ?", id).Find(&items)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	// returns 404 if either row doesn't exist or if the user doesn't own it
	if res.RowsAffected == 0 {
		return errors.ErrEmptyResponse
	}

	return nil
}

func getBrands(brands *[]Brand, userId uint) error {
	res := db.Where("user_id = ?", userId).Find(&brands)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return errors.ErrEmptyResponse
	}

	return nil
}

func (b *Brand) upsert() error {
	res := db.Save(b)
	// TODO: handle potential errors
	return res.Error
}

func (b *Brand) del() error {
	// delete brand
	res := db.Where("id = ? and user_id = ?", b.ID, b.UserID).Delete(b)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	// returns 404 if either row doesn't exist or if the user doesn't own it
	if res.RowsAffected == 0 {
		return errors.ErrNotFound
	}

	return nil
}

func getItems(items *[]SavedItem, userId uint) error {
	res := db.Where("user_id = ?", userId).Preload("Brand").Find(&items)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return errors.ErrEmptyResponse
	}

	return nil
}

func (i *SavedItem) upsert() error {
	res := db.Save(i)
	// TODO: handle potential errors
	return res.Error
}

func (i *SavedItem) del() error {
	res := db.Where("id = ? and user_id = ?", i.ID, i.UserID).Delete(i)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	// returns 404 if either row doesn't exist or if the user doesn't own it
	if res.RowsAffected == 0 {
		return errors.ErrNotFound
	}

	return nil
}
