/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package item

import (
	"gorm.io/gorm"
	d "vidhukant.com/openbills/db"
	"vidhukant.com/openbills/user"
)

var db *gorm.DB
func init() {
	db = d.DB

	db.AutoMigrate(&SavedItem{}, &Brand{})
}

type Brand struct {
	gorm.Model
	UserID uint      `json:"-"`
	User   user.User `json:"-"`
	Name   string
}

type Item struct {
	Name          string
	Description   string
	HSN           string
	UnitOfMeasure string // TODO: probably has to be a custom type
	UnitPrice     string // float
	GSTPercentage string // float
}

type SavedItem struct {
	gorm.Model
	Item
	BrandID            uint
	Brand              Brand
	UserID             uint      `json:"-"`
	User               user.User `json:"-"`
}
