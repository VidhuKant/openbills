/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package item

import (
	"strconv"
	"strings"

	"vidhukant.com/openbills/errors"
)

func (b *Brand) validate() error {
	// trim whitespaces
	b.Name = strings.TrimSpace(b.Name)

	if b.Name == "" {
		return errors.ErrEmptyBrandName
	}

	// make sure GSTIN is unique
	var count int64
	err := db.Model(&Brand{}).
		Select("name").
		Where("name = ? and user_id = ?", b.Name, b.UserID).
		Count(&count).
		Error

	if err != nil {
		return err
	}

	if count > 0 {
		return errors.ErrNonUniqueBrandName
	}

	return nil
}

func (i *SavedItem) validate() error {
	// trim whitespaces
	i.Name = strings.TrimSpace(i.Name)
	i.Description = strings.TrimSpace(i.Description)
	i.HSN = strings.TrimSpace(i.HSN)

	var err error

	// check if UnitPrice is float
	_, err = strconv.ParseFloat(i.UnitPrice, 64)
	if err != nil && strings.TrimSpace(i.UnitPrice) != "" {
		return errors.ErrInvalidUnitPrice
	}

	// check if GSTPercentage is float
	_, err = strconv.ParseFloat(i.GSTPercentage, 64)
	if err != nil && strings.TrimSpace(i.GSTPercentage) != "" {
		return errors.ErrInvalidGSTPercentage
	}

	// check if item with same name and brand already exists
	var count int64
	err = db.Model(&SavedItem{}).
		Select("name, brand_id").
		Where("brand_id = ? and name = ? and user_id = ?", i.BrandID, i.Name, i.UserID).
		Count(&count).
		Error

	if err != nil {
		return err
  }

	if count != 0 {
		return errors.ErrNonUniqueBrandItem
	}

	return nil
}

func checkBrandOwnership(brandId, userId uint) error {
	var brand Brand
	err := db.
		Select("id", "user_id").
		Where("id = ?", brandId).
		Find(&brand).
		Error

	// TODO: handle potential errors
	if err != nil {
		return err
  }

	// brand doesn't exist
	if brand.ID == 0 {
		return errors.ErrNotFound
	}

	// user doesn't own this brand
	if brand.UserID != userId {
		return errors.ErrForbidden
	}

	return nil
}

func checkItemOwnership(itemId, userId uint) error {
	var item SavedItem
	err := db.
		Select("id", "user_id").
		Where("id = ?", itemId).
		Find(&item).
		Error

	// TODO: handle potential errors
	if err != nil {
		return err
  }

	// item doesn't exist
	if item.ID == 0 {
		return errors.ErrNotFound
	}

	// user doesn't own this item
	if item.UserID != userId {
		return errors.ErrForbidden
	}

	return nil
}
