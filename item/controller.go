/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package item

import (
	e "vidhukant.com/openbills/errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func handleGetBrandItems (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		return
	}

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	var items []SavedItem
	err = getBrandItems(&items, uint(id), userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": items,
	})
}

func handleGetBrands (ctx *gin.Context) {
	var brands []Brand

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	err := getBrands(&brands, userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": brands,
	})
}

func handleSaveBrand (ctx *gin.Context) {
	var brand Brand
	ctx.Bind(&brand)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	brand.UserID = userId

	err := brand.upsert()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": brand,
	})
}

func handleDelBrand (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		return
	}

	var brand Brand
	brand.ID = uint(id)


	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	brand.UserID = userId

	err = checkBrandOwnership(brand.ID, brand.UserID)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	err = brand.del()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}

func handleGetItems (ctx *gin.Context) {
	var items []SavedItem

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	err := getItems(&items, userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": items,
	})
}

func handleSaveItem (ctx *gin.Context) {
	var item SavedItem
	ctx.Bind(&item)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	item.UserID = userId

	err := item.upsert()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": item,
	})
}

func handleDelItem (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		return
	}

	var item SavedItem
	item.ID = uint(id)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	item.UserID = userId

	err = checkItemOwnership(item.ID, item.UserID)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	err = item.del()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}
