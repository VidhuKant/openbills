/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package customer

import (
	e "vidhukant.com/openbills/errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func handleGetSingleCustomer (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		return
	}

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	var customer Customer

	err = getCustomer(&customer, uint(id))
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	if customer.UserID != userId {
		ctx.Error(e.ErrForbidden)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": customer,
	})
}

func handleGetCustomers (ctx *gin.Context) {
	var customers []Customer

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	err := getCustomers(&customers, userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": customers,
	})
}

func handleSaveCustomer (ctx *gin.Context) {
	var customer Customer
	ctx.Bind(&customer)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	customer.UserID = userId

	err := customer.upsert()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": customer,
	})
}

func handleDelCustomer (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		return
	}

	var customer Customer
	customer.ID = uint(id)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	customer.UserID = userId

	err = checkCustomerOwnership(customer.ID, customer.UserID)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	err = customer.del()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}
