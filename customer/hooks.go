/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package customer

import (
	"gorm.io/gorm"
	"vidhukant.com/openbills/errors"
)

func (c *Customer) BeforeSave(tx *gorm.DB) error {
	var err error

	err = c.validate()
	if err != nil {
		return err
	}

	return nil
}

func (c *Customer) BeforeDelete(tx *gorm.DB) error {
  // if ID is 0, customer won't be deleted
	if c.ID == 0 {
		return errors.ErrNoWhereCondition
	}

	var err error

	// delete billing address
	err = db.Where("customer_id = ?", c.ID).Delete(&CustomerBillingAddress{}).Error
	if err != nil {
		return err
	}

	// delete shipping addresses
	err = db.Where("customer_id = ?", c.ID).Delete(&CustomerShippingAddress{}).Error
	if err != nil {
		return err
	}

	return nil
}
