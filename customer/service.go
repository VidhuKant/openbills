/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package customer

import (
	e "vidhukant.com/openbills/errors"
)

func getCustomer(customer *Customer, id uint) error {
	res := db.Preload("BillingAddress").Preload("ShippingAddresses").Find(&customer, id)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}

func getCustomers(customers *[]Customer, userId uint) error {
	res := db.Where("user_id = ?", userId).Find(&customers)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrEmptyResponse
	}

	return nil
}

func (c *Customer) upsert() error {
	res := db.Save(c)
	// TODO: handle potential errors
	return res.Error
}

func (c *Customer) del() error {
	res := db.Where("id = ? and user_id = ?", c.ID, c.UserID).Delete(c)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	// returns 404 if either row doesn't exist or if the user doesn't own it
	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}
