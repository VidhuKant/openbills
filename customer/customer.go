/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023-2024  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package customer

import (
	"gorm.io/gorm"
	d "vidhukant.com/openbills/db"
  u "vidhukant.com/openbills/util"
	"vidhukant.com/openbills/user"
)

var db *gorm.DB
func init() {
	db = d.DB

	db.AutoMigrate(&Customer{}, &CustomerBillingAddress{}, &CustomerShippingAddress{})
}

type CustomerBillingAddress struct {
	gorm.Model
	u.Address
	CustomerID  uint
}

type CustomerShippingAddress struct {
	gorm.Model
	u.Address
	CustomerID  uint
}

type Customer struct {
	gorm.Model
	UserID            uint      `json:"-"`
	User              user.User `json:"-"`
	Name              string
	Gstin             string
	ContactName       string
	Phone             string
	Email             string
	Website           string
	BillingAddress    CustomerBillingAddress
	ShippingAddresses []CustomerShippingAddress
}
