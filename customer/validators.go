/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023-2024  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package customer

import (
	"strings"
	"vidhukant.com/openbills/errors"
	u "vidhukant.com/openbills/util"
	e "errors"
)

// NOTE: very inefficient and really really really dumb but it works
// TODO: find a better (or even a remotely good) way
func checkDuplicate(field, value string, userId uint) error {
	if value != "" {
		var count int64
		err := db.Model(&Customer{}).
			Where("user_id = ? and " + field + " = ?", userId, value).
			Count(&count).
			Error

		if err != nil {
			return err
		}

		if count > 0 {
			switch(field) {
			case "phone":
				return errors.ErrNonUniquePhone
			case "email":
				return errors.ErrNonUniqueEmail
			case "website":
				return errors.ErrNonUniqueWebsite
			case "gstin":
        return errors.ErrNonUniqueGSTIN
			default:
				return e.New(field + " is not unique")
			}
		}
	}

	return nil
}

func (c *Customer) validate() error {
	// trim whitespaces
	c.Name = strings.TrimSpace(c.Name)
	c.Gstin = strings.TrimSpace(c.Gstin)
	c.ContactName = strings.TrimSpace(c.Name)
	c.Phone = strings.TrimSpace(c.Phone)
	c.Email = strings.TrimSpace(c.Email)
	c.Website = strings.TrimSpace(c.Website)

	// don't validate if GSTIN is empty
	if c.Gstin != "" {
		if !u.ValidateGstin(c.Gstin) {
			return errors.ErrInvalidGSTIN
		}
	}

	// don't validate email if empty
	if c.Email != "" {
		if !u.ValidateEmail(c.Email) {
			return errors.ErrInvalidEmail
		}
	}

	// don't validate phone if empty
	if c.Phone != "" {
		if !u.ValidatePhone(c.Phone) {
			return errors.ErrInvalidPhone
		}
	}

	// don't validate website if empty
	if c.Website != "" {
		if !u.ValidateWebsite(c.Website) {
			return errors.ErrInvalidWebsite
		}
	}

	var err error
	for _, i := range [][]string{{"phone", c.Phone}, {"email", c.Email}, {"website", c.Website}, {"gstin", c.Gstin}} {
		err = checkDuplicate(i[0], i[1], c.UserID)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkCustomerOwnership(customerId, userId uint) error {
	var customer Customer
	err := db.
		Select("id", "user_id").
		Where("id = ?", customerId).
		Find(&customer).
		Error

	// TODO: handle potential errors
	if err != nil {
		return err
  }

	// customer doesn't exist
	if customer.ID == 0 {
		return errors.ErrNotFound
	}

	// user doesn't own this customer
	if customer.UserID != userId {
		return errors.ErrForbidden
	}

	return nil
}
