/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	// used internally
	_ "vidhukant.com/openbills/conf"
	_ "vidhukant.com/openbills/db"

	// middlewares, etc
  "vidhukant.com/openbills/errors"

	// routes
	"vidhukant.com/openbills/user"
	"vidhukant.com/openbills/auth"
	"vidhukant.com/openbills/customer"
	"vidhukant.com/openbills/item"
	"vidhukant.com/openbills/invoice"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"log"
)

const OPENBILLS_VERSION = "v0.6.1"

func init() {
	if !viper.GetBool("debug_mode") {
		gin.SetMode(gin.ReleaseMode)
	}
}

func main() {
	r := gin.New()
	r.SetTrustedProxies([]string{"127.0.0.1"})

	r.GET("/info", serverInfo)

	api := r.Group("/api")
	api.Use(errors.ErrResponse())
	{
		auth.Routes(api)
	}

	protected := api.Group("/")
	protected.Use(auth.Authorize())
	{
		user.Routes(protected)
		customer.Routes(protected)
		item.Routes(protected)
		invoice.Routes(protected)
	}

	log.Printf("\x1b[46m\x1b[30m[info]\x1b[0m Running OpenBills Server %s\n", OPENBILLS_VERSION)
	r.Run(":" + viper.GetString("port"))
}
