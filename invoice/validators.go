/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package invoice

import (
	"vidhukant.com/openbills/errors"
)

func (i *Invoice) validate() error {
	var count int64
	err := db.Model(&Invoice{}).
		Where("user_id = ? and invoice_number = ?", i.UserID, i.InvoiceNumber).
		Count(&count).
		Error

	if err != nil {
		return err
	}

	if count > 0 {
		return errors.ErrNonUniqueInvoiceNumber
	}

	return nil
}

func isDraft(invoiceId uint) (bool, error) {
	var invoice Invoice
	err := db.
		Select("id", "is_draft").
		Where("id = ?", invoiceId).
		Find(&invoice).
		Error

	// TODO: handle potential errors
	if err != nil {
		return invoice.IsDraft, err
  }

	// invoice doesn't exist
	if invoice.ID == 0 {
		return invoice.IsDraft, errors.ErrNotFound
	}

	return invoice.IsDraft, nil
}

func checkInvoiceOwnership(invoiceId, userId uint) error {
	var invoice Invoice
	err := db.
		Select("id", "user_id").
		Where("id = ?", invoiceId).
		Find(&invoice).
		Error

	// TODO: handle potential errors
	if err != nil {
		return err
  }

	// invoice doesn't exist
	if invoice.ID == 0 {
		return errors.ErrNotFound
	}

	// user doesn't own this invoice
	if invoice.UserID != userId {
		return errors.ErrForbidden
	}

	return nil
}
