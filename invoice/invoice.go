/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023-2024  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package invoice

import (
	"gorm.io/gorm"
	d "vidhukant.com/openbills/db"
	"vidhukant.com/openbills/user"
	u "vidhukant.com/openbills/util"
	i "vidhukant.com/openbills/item"
	"time"
)

var db *gorm.DB
func init() {
	db = d.DB

	db.AutoMigrate(&Invoice{}, &InvoiceItem{}, &InvoiceBillingAddress{}, &InvoiceShippingAddress{})
}

type InvoiceBillingAddress struct {
	gorm.Model
	u.Address
	InvoiceID uint
}

type InvoiceShippingAddress struct {
	gorm.Model
	u.Address
	InvoiceID uint
}

type InvoiceItem struct {
	gorm.Model
	i.Item
	InvoiceID uint
	BrandName string
	Quantity  string // float
}

type Invoice struct {
	gorm.Model
	UserID          uint      `json:"-"`
	User            user.User `json:"-"`
	InvoiceDate     time.Time
	InvoiceNumber   uint
	BillingAddress  InvoiceBillingAddress
	ShippingAddress InvoiceShippingAddress
	IsDraft         bool
	Items           []InvoiceItem

  // issuer and customer details are stored here 
  // because they are NOT intended to ever change
  IssuerFirmName      string
  IssuerFirmAddress   string
  IssuerFirmGstin     string
  IssuerFirmPhone     string
  IssuerFirmEmail     string
  IssuerFirmWebsite   string
	CustomerName        string
	CustomerGstin       string
	CustomerContactName string
	CustomerPhone       string
	CustomerEmail       string
	CustomerWebsite     string

	// Transporter     Transporter
	// DueDate         string
	// TransactionID   string
}
