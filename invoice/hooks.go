/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package invoice

import (
	"gorm.io/gorm"
	"vidhukant.com/openbills/errors"
)

func (i *Invoice) BeforeSave(tx *gorm.DB) error {
	var err error

	err = i.validate()
	if err != nil {
		return err
	}

	return nil
}

func (i *Invoice) BeforeDelete(tx *gorm.DB) error {
  // if ID is 0, invoice won't be deleted
	if i.ID == 0 {
		return errors.ErrNoWhereCondition
	}

	var err error

	// delete billing address
	err = db.Where("invoice_id = ?", i.ID).Delete(&InvoiceBillingAddress{}).Error
	if err != nil {
		return err
	}

	// delete shipping address
	err = db.Where("invoice_id = ?", i.ID).Delete(&InvoiceShippingAddress{}).Error
	if err != nil {
		return err
	}

	return nil
}

func (i *InvoiceItem) BeforeSave(tx *gorm.DB) error {
	var err error

	isDraft, err := isDraft(i.InvoiceID)
	if err != nil {
		return err
	}

	if !isDraft {
		return errors.ErrCannotEditInvoice
	}

	return nil
}

func (i *InvoiceItem) BeforeDelete(tx *gorm.DB) error {
	var err error

	isDraft, err := isDraft(i.InvoiceID)
	if err != nil {
		return err
	}

	if !isDraft {
		return errors.ErrCannotEditInvoice
	}

	return nil
}
