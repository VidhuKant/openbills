/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package invoice

import (
	"github.com/gin-gonic/gin"
)

func Routes(route *gin.RouterGroup) {
	g := route.Group("/invoice")
	{
		g.GET("/", handleGetInvoices)
		g.GET("/draft", handleGetDrafts)
		g.GET("/:id", handleGetSingleInvoice)
		g.POST("/", handleSaveInvoice)
		g.DELETE("/:id", handleDelInvoice)
		g.GET("/:id/item", handleGetInvoiceItems)
		g.POST("/:id/item", addItem)
		g.DELETE("/item/:id", removeItem)
	}
}
