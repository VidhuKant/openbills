/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package invoice

import (
	e "vidhukant.com/openbills/errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func handleGetSingleInvoice (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		ctx.Abort()
		return
	}

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	var invoice Invoice

	err = getInvoice(&invoice, uint(id))
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	if invoice.UserID != userId {
		ctx.Error(e.ErrForbidden)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": invoice,
	})
}

func handleGetInvoices (ctx *gin.Context) {
	var invoices []Invoice

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	err := getInvoices(&invoices, userId, false)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": invoices,
	})
}

func handleGetDrafts (ctx *gin.Context) {
	var invoices []Invoice

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	err := getInvoices(&invoices, userId, true)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": invoices,
	})
}

func handleSaveInvoice (ctx *gin.Context) {
	var invoice Invoice
	ctx.Bind(&invoice)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	invoice.UserID = userId

	// if invoice number is 0, generate one!
	if invoice.InvoiceNumber == 0 {
		n, err := getNewInvoiceNumber(invoice.UserID)

		if err != nil {
			ctx.Error(err)
			ctx.Abort()
			return
		}

		invoice.InvoiceNumber = n
	}

	err := invoice.upsert()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": invoice,
	})
}

func handleDelInvoice (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		ctx.Abort()
		return
	}

	var invoice Invoice
	invoice.ID = uint(id)

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)
	invoice.UserID = userId

	err = checkInvoiceOwnership(invoice.ID, invoice.UserID)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	err = invoice.del()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}

// get items belonging to a certain invoice
func handleGetInvoiceItems (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		ctx.Abort()
		return
	}

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)


	err = checkInvoiceOwnership(uint(id), userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	var items []InvoiceItem
	err = getInvoiceItems(&items, uint(id))
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": items,
	})
}

func addItem (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		ctx.Abort()
		return
	}

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	var item InvoiceItem
	ctx.Bind(&item)

	item.InvoiceID = uint(id)

	err = checkInvoiceOwnership(item.InvoiceID, userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	err = item.upsert()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": item,
	})
}

func removeItem (ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.Error(e.ErrInvalidID)
		ctx.Abort()
		return
	}

	uId, ok := ctx.Get("UserID")
	if !ok {
		ctx.Error(e.ErrUnauthorized)
		ctx.Abort()
		return
	}

	userId := uId.(uint)

	var item InvoiceItem
	item.ID = uint(id)

	invoiceId, err := getItemInvoice(item.ID, userId)
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	item.InvoiceID = invoiceId

	err = item.del()
	if err != nil {
		ctx.Error(err)
		ctx.Abort()
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data": item,
	})
}
