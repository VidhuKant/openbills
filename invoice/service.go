/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package invoice

import (
	e "vidhukant.com/openbills/errors"
)

// returns greatest invoice number + 1
func getNewInvoiceNumber(userId uint) (uint, error) {
	var i uint

	// check if number of invoices is 0
	var count int64
	err := db.Model(&Invoice{}).
		Where("user_id = ?", userId).
		Count(&count).
		Error

	if err != nil {
		return i, err
	}

	// if no records exist, then invoice number should be 1
	if count == 0 {
		return 1, nil
	}

	// if records exist, get max invoice number
	// NOTE: if there are gaps in invoice numbers,
	// they won't be filled and the series would continue
	// from the greatest invoice number.
	row := db.Model(&Invoice{}).Where("user_id = ?", userId).Select("max(invoice_number)").Row()
	err = row.Scan(&i)

	return i + 1, err
}

func getInvoice(invoice *Invoice, id uint) error {
	res := db.Preload("BillingAddress").Preload("ShippingAddress").Preload("Items").Find(&invoice, id)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}

func getInvoices(invoices *[]Invoice, userId uint, isDraft bool) error {
	res := db.Where("user_id = ? and is_draft = ?", userId, isDraft).Find(&invoices)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrEmptyResponse
	}

	return nil
}

func getInvoiceItems(items *[]InvoiceItem, invoiceId uint) error {
	res := db.Where("invoice_id = ?", invoiceId).Find(&items)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	if res.RowsAffected == 0 {
		return e.ErrEmptyResponse
	}

	return nil
}

// TODO: route to only get the invouce's items

func (i *Invoice) upsert() error {
	res := db.Save(i)
	// TODO: handle potential errors
	return res.Error
}

func (i *Invoice) del() error {
	res := db.Where("id = ? and user_id = ?", i.ID, i.UserID).Delete(i)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	// returns 404 if either row doesn't exist or if the user doesn't own it
	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}

// also checks for ownership
func getItemInvoice(itemId, userId uint) (uint, error) {
	var invoiceId uint
	res := db.
		Model(&InvoiceItem{}).
		Select("invoice_id").
		Where("id = ?", itemId).
		Find(&invoiceId)

	// TODO: handle potential errors
	if res.Error != nil {
		return invoiceId, res.Error
	}

	if res.RowsAffected == 0 {
		return invoiceId, e.ErrNotFound
	}

	err := checkInvoiceOwnership(invoiceId, userId)

	if err != nil {
		return invoiceId, err
	}

	return invoiceId, nil
}

func (i *InvoiceItem) del() error {
	res := db.Delete(i)

	// TODO: handle potential errors
	if res.Error != nil {
		return res.Error
	}

	// returns 404 if either row doesn't exist or if the user doesn't own it
	if res.RowsAffected == 0 {
		return e.ErrNotFound
	}

	return nil
}

func (i *InvoiceItem) upsert() error {
	res := db.Save(i)
	// TODO: handle potential errors
	return res.Error
}
