/* openbills - Server for web based Libre Billing Software
 * Copyright (C) 2023  Vidhu Kant Sharma <vidhukant@vidhukant.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package auth

import (
	"vidhukant.com/openbills/errors"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"strings"
	"time"
)

func getBearerToken(header []string) (string, error) {
	if len(header) > 0 {
		s := strings.Split(header[0], "Bearer ")
		if len(s) == 2 {
			return s[1], nil
		} else {
			return "", errors.ErrInvalidAuthHeader
		}
	} else {
		return "", errors.ErrInvalidAuthHeader
	}
}

func Authorize() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		bearerToken, err := getBearerToken(ctx.Request.Header["Authorization"])
		if err != nil {
			ctx.Error(err)
			ctx.Abort()
			return
		}

		tk, _ := jwt.ParseWithClaims(bearerToken, &AuthClaims{}, func (token *jwt.Token) (interface{}, error) {
			return []byte(AUTH_KEY), nil
		})

		claims, ok := tk.Claims.(*AuthClaims)
		if !ok {
			ctx.Error(errors.ErrInvalidAuthHeader)
			ctx.Abort()
			return
		}

		if !tk.Valid {
			eat := claims.ExpiresAt.Unix()
			if eat != 0 && eat < time.Now().Unix() {
				ctx.Error(errors.ErrSessionExpired)
			} else {
				ctx.Error(errors.ErrUnauthorized)
			}

			ctx.Abort()
			return
		}

		ctx.Set("UserID", claims.UserID)

		ctx.Next()
	}
}
