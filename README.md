# OpenBills

OpenBills is a web based libre billing software.

## Licence
Licenced under GNU General Public Licence

GNU GPL License: [LICENSE](https://dev.vidhukant.xyz/openbills/tree/LICENSE)

Copyright (c) 2023 Vidhu Kant Sharma
